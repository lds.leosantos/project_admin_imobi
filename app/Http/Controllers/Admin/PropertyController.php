<?php

namespace LaraDev\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use LaraDev\Http\Controllers\Controller;
use LaraDev\Property;
use LaraDev\Http\Requests\Admin\Property as PropertyRequest;
use LaraDev\PropertyImage;
use LaraDev\Support\Cropper;
use LaraDev\User;

class PropertyController extends Controller
{
    public function index()
    {
        $properties = Property::orderBy('id','DESC')->get();
        return view('admin.properties.index',[
            'properties' => $properties
        ]);
    }


    public function create()
    {
        $users = User::orderBy('name')->get();

        return view('admin.properties.create',[
            'users'=>$users
        ]);
    }


    public function store(PropertyRequest $request)
    {
        $createProperty = Property::create($request->all());

        $validator = Validator::make($request->only('files'), ['files.* '=> 'image']);
        //verificar nas atuailzaçoes se esta negado ou não

        if($validator->fails() === true){
            return redirect()->back()->withInput()->with(['color'=>'orange','message'=>'todos as images devem ser do tipo jpg,
            jpeg,png,svg']);
        }
        if($request->allFiles()){
            foreach ($request->allFiles()['files'] as  $image) {
                $propertyImage = new PropertyImage();
                $propertyImage->property = $createProperty->id;
                $propertyImage->path = $image->store('properties/' . $createProperty->id);
                $propertyImage->save();
                unset($propertyImage);
            }
        }

        return redirect()->route('admin.properties.edit', [
            'property' => $createProperty->id
        ])->with(['color' => 'green', 'message' => 'Imovel cadastrado com sucesso']);
    }

    public function show($id)
    {
    }


    public function edit($id)
    {
        $property = Property::where('id', $id)->first();
        $users = User::orderBy('name')->get();

        return view('admin.properties.edit', [
            'property' => $property,
            'users'=>$users
        ]);
    }


    public function update(PropertyRequest $request, $id)
    {

        $property = Property::where('id', $id)->first();
        $property->fill($request->all());

        //Verificando os campos checked se esta marcado (1) ou não (0) ;
        $property->setSaleAttribute($request->sale);
        $property->setRentAttribute($request->rent);
        $property->setAirConditioningAttribute($request->air_conditioning);
        $property->setBarAttribute($request->bar);
        $property->setLibraryAttribute($request->library);
        $property->setBarbecueGrillAttribute($request->barbecue_grill);
        $property->setAmericanKitchenAttribute($request->american_kitchen);
        $property->setFittedKitchenAttribute($request->fitted_kitchen);
        $property->setPantryAttribute($request->pantry);
        $property->setEdiculeAttribute($request->edicule);
        $property->setOfficeAttribute($request->office);
        $property->setBathtubAttribute($request->bathtub);
        $property->setFireplaceAttribute($request->fireplace);
        $property->setLavatoryAttribute($request->lavatory);
        $property->setfurnishedAttribute($request->furnished);
        $property->setPoolAttribute($request->pool);
        $property->setSteamRoomAttribute($request->steam_room);
        $property->setViewOfTheSeaAttribute($request->view_of_the_sea);

        $property->save();


        $validator = Validator::make($request->only('files'), ['files.*' => 'image']);

        //verificar nas atuailzaçoes se esta negado ou não
        if($validator->fails() === true){
            return redirect()->back()->withInput()->with(['color'=>'orange','message'=>'todos as images devem ser do tipo jpg,
            jpeg,png,svg']);
        }


        if($request->allFiles()){
            foreach ($request->allFiles()['files'] as  $image) {
                $propertyImage = new PropertyImage();
                $propertyImage->property = $property->id;
                $propertyImage->path = $image->store('properties/' . $property->id);
                $propertyImage->save();
                unset($propertyImage);
            }
        }


        return redirect()->route('admin.properties.edit', [
            'property' => $property->id
        ])->with(['color' => 'green', 'message' => 'Imovel Alterado com sucesso']);
    }


    public function destroy($id)
    {
        //
    }

    public function imageSetCover(Request $request)
    {

        $imageSetCover  = PropertyImage::where('id',$request->image)->first();

        $allImage  = PropertyImage::where('property',$imageSetCover->property)->get();

        foreach ($allImage as $image){
            $image->cover = null;
            $image->save();
        }
        $imageSetCover->cover = true;
        $imageSetCover->save();


        $json = [
            'success'=>true
        ];
        return response()->json($json);
    }

    public function imageRemove(Request $request)
    {
        $imageDelete  = PropertyImage::where('id',$request->image)->first();

        Storage::delete($imageDelete->path);
        Cropper::flush($imageDelete->path);

        $imageDelete->delete();

        $json = [
            'success'=>true
        ];

        return response()->json($json);
    }
}
