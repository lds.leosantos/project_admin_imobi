<?php


use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function () {

    /** formulário de login */
    Route::get('/', 'AuthController@showLoginForm')->name('login');
    Route::post('login', 'AuthController@login')->name('login.do');

    /** rotas protegidas */
    Route::group(['middleware' => ['auth']], function () {
        /**dashboard home*/
        Route::get('home', 'AuthController@home')->name('home');
        Route::get('users/team', 'UserController@team')->name('users.team');
        /**users */
        Route::resource('users', 'UserController');
        /**empresas*/
        Route::resource('companies', 'CompanyController');
        /**imoves*/
        Route::post('properties/image-set-cover', 'PropertyController@imageSetCover')->name('properties.imageSetCover');
        Route::delete('properties/image-remove', 'PropertyController@imageRemove')->name('properties.imageRemove');
        Route::resource('properties',   'PropertyController');
        /**Contratos */
  

        Route::post('contracts/get-data-owner','ContractController@getDataOwner')->name('contracts.getDataOwner');
        Route::post('contracts/get-data-acquirer','ContractController@getDataAcquirer')->name('contracts.getDataAcquirer');
        Route::post('contracts/get-data-property','ContractController@getDataProperty')->name('contracts.getDataProperty');

        Route::resource('contracts', 'ContractController');

    });
    /** Logout*/
    Route::get('logout', 'AuthController@logout')->name('logout');
});
