<?php $__env->startSection('content'); ?>
<section class="dash_content_app">

    <header class="dash_content_app_header">
        <h2 class="icon-users">Time</h2>

        <div class="dash_content_app_header_actions">
            <nav class="dash_content_app_breadcrumb">
                <ul>
                    <li><a href="">Dashboard</a></li>
                    <li class="separator icon-angle-right icon-notext"></li>
                    <li><a href="" class="text-orange">Time</a></li>
                </ul>
            </nav>

            <a href="" class="btn btn-orange icon-user-plus ml-1">Criar Usuário</a>
        </div>
    </header>

    <div class="dash_content_app_box">
        <section class="app_users_home">
            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <article class="user radius">
                    <div class="cover"
                        style="background-size: cover; background-image: url(<?php echo e($user->url_cover); ?>);"></div>
                    <h4><?php echo e($user->name); ?></h4>

                    <div class="info">
                        <p><?php echo e($user->email); ?></p>
                        <p>Desde <?php echo e(date('d/m/Y'), strtotime($user->last_login_at)); ?></p>
                    </div>

                    <div class="actions">
                        <a class="icon-cog btn btn-orange" href="<?php echo e(route('admin.users.edit', ['user'=>$user->id])); ?>" 
                            title="">Gerenciar</a>
                    </div>
                </article>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </section>
    </div>
</section>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>