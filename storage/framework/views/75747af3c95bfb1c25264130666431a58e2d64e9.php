<?php $__env->startSection('content'); ?>
<section class="dash_content_app">
    <header class="dash_content_app_header">
        <h2 class="icon-search">Listar Imóveis </h2>
        <div class="dash_content_app_header_actions">
            <nav class="dash_content_app_breadcrumb">
                <ul>
                    <li><a href="<?php echo e(route('admin.home')); ?>">Dashboard</a></li>
                    <li class="separator icon-angle-right icon-notext"></li>
                    <li><a href="">Imóveis</a></li>
                    <li class="separator icon-angle-right icon-notext"></li>
                    <li><a href="<?php echo e(route('admin.properties.create')); ?>" class="text-orange">Cadastrar Imóvel</a></li>
                </ul>
            </nav>
        </div>
    </header>

    <?php echo $__env->make('admin.properties.filter', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="dash_content_app_box">
        <div class="dash_content_app_box_stage">
            <div class="realty_list">
                <div class="realty_list">
                    <?php if(!empty ($properties)): ?>
                    <?php $__currentLoopData = $properties; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $property): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="realty_list_item mb-2">
                        <div class="realty_list_item_actions_stats">
                            <img src="<?php echo e($property->cover()); ?>" alt="">
                            <ul>
                                <?php if($property->sale == true && !empty($property->sale_price)): ?>
                                <li>Venda: <?php echo e($property->sale_price); ?></li>
                                <?php endif; ?>

                                <?php if($property->rent == true && !empty($property->rent_price)): ?>
                                <li>Aluguel: <?php echo e($property->rent_price); ?></li>
                                <?php endif; ?>
                            </ul>
                        </div>

                        <div class="realty_list_item_content">
                            <h4># <?php echo e($property->id); ?> - <?php echo e($property->category); ?> - <?php echo e($property->type); ?></h4>

                            <div class="realty_list_item_card">
                                <div class="realty_list_item_card_image">
                                    <span class="icon-realty-location"></span>
                                </div>
                                <div class="realty_list_item_card_content">
                                    <span class="realty_list_item_description_title">Bairro:</span>
                                    <span
                                        class="realty_list_item_description_content"><?php echo e($property->neighborhood); ?></span>
                                </div>
                            </div>

                            <div class="realty_list_item_card">
                                <div class="realty_list_item_card_image">
                                    <span class="icon-realty-util-area"></span>
                                </div>
                                <div class="realty_list_item_card_content">
                                    <span class="realty_list_item_description_title">Área
                                        Útil:</span>
                                    <span
                                        class="realty_list_item_description_content"><?php echo e($property->area_util); ?>&sup2;</span>
                                </div>
                            </div>

                            <div class="realty_list_item_card">
                                <div class="realty_list_item_card_image">
                                    <span class="icon-realty-bed"></span>
                                </div>
                                <div class="realty_list_item_card_content">
                                    <span
                                        class="realty_list_item_desscription_title">Domitórios:</span>
                                    <span class="realty_list_item_description_content"><?php echo e($property->bedrooms + $property->suites); ?>

                                        Quartos<br><span> sendo <?php echo e($property->suites); ?> suite(s)</span></span>
                                </div>
                            </div>
                            <div class="realty_list_item_card">
                                <div class="realty_list_item_card_image">
                                    <span class="icon-realty-garage"></span>
                                </div>
                                <div class="realty_list_item_card_content">
                                    <span
                                        class="realty_list_item_description_title">Garagem:</span>
                                    <span class="realty_list_item_description_content"><?php echo e($property->garage + $property->garage_covered); ?>

                                        Vagas<br><span>Sendo <?php echo e($property->garage_covered); ?> cobertas</span></span>
                                </div>
                            </div>

                        </div>

                        <div class="realty_list_item_actions">
                            <ul>
                                <li class="icon-eye">1234 Visualizações</li>
                            </ul>
                            <div>
                                <a href="" class="btn btn-blue icon-eye">Visualizar Imóvel</a>
                                <a href="<?php echo e(route('admin.properties.edit',['property'=>$property->id ])); ?>" class="btn btn-green icon-pencil-square-o">Editar
                                    Imóvel</a>
                            </div>
                        </div>
                    </div> 
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  
                        <?php else: ?> 
                        <div class="no-content">Não foram encontrados registros!</div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>