<?php $__env->startSection('content'); ?>
    <div style="flex-basis: 100%;">
        <section class="dash_content_app">
            <header class="dash_content_app_header">
                <h2 class="icon-tachometer">Dashboard</h2>
            </header>
            <div class="dash_content_app_box">
                <section class="app_dash_home_stats">
                    <article class="control radius">
                        <h4 class="icon-users">Clientes</h4>
                        <p><b>Locadores:</b> <?php echo e($lessors); ?></p>
                        <p><b>Locatários:</b> <?php echo e($lessees); ?></p>
                        <p><b>Time: </b><?php echo e($teams); ?></p>
                    </article>
                    <article class="blog radius">
                        <h4 class="icon-home">Imóveis</h4>
                        <p><b>Disponíveis:</b><?php echo e($propertiesAvailable); ?> </p>
                        <p><b>Locados:</b> <?php echo e($propertiesUnavailable); ?></p>
                        <p><b>Total:</b> <?php echo e($propertiesTotal); ?></p>
                    </article>
                    <article class="users radius">
                        <h4 class="icon-file-text">Contratos</h4>
                        <p><b>Pendentes:</b> <?php echo e($contractPending); ?></p>
                        <p><b>Ativos:</b> <?php echo e($contractActive); ?></p>
                        <p><b>Cancelados:</b> <?php echo e($contractCanceled); ?></p>
                        <p><b>Total:</b> <?php echo e($contractTotal); ?></p>
                    </article>
                </section>
            </div>
        </section>
        <section class="dash_content_app" style="margin-top: 40px;">
            <header class="dash_content_app_header">
                <h2 class="icon-tachometer">Últimos Contratos Cadastrados</h2>
            </header>

            <div class="dash_content_app_box">
                <div class="dash_content_app_box_stage">
                    <table id="dataTable" class="nowrap hover stripe" width="100" style="width: 100% !important;">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Locador</th>
                            <th>Locatário</th>
                            <th>Negócio</th>
                            <th>Início</th>
                            <th>Vigência</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $contracts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $contract): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><a href="<?php echo e(route('admin.contracts.edit',['contract'=>$contract->id])); ?>"  class="text-orange"><?php echo e($contract->id); ?></a></td>
                                <td><a href="<?php echo e(route('admin.users.edit',['user' =>$contract->ownerObject->id])); ?>" class="text-orange"><?php echo e($contract->ownerObject->name); ?></a></td>
                                <td><a href="<?php echo e(route('admin.users.edit',['user' =>$contract->acquirerObject->id])); ?>" class="text-orange"><?php echo e($contract->acquirerObject->name); ?></a></td>
                                <td> <?php echo e(($contract->sale == true ? 'Venda' : 'Locação')); ?> </td>
                                <td><?php echo e($contract->start_at); ?></td>
                                <td><?php echo e($contract->deadline); ?></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <section class="dash_content_app" style="margin-top: 40px;">
            <header class="dash_content_app_header">
                <h2 class="icon-tachometer">Últimos Imóveis Cadastrados</h2>
            </header>
            <div class="dash_content_app_box">
                <div class="dash_content_app_box_stage">
                    <div class="realty_list">
                        <?php if(!empty ($properties)): ?>
                        <?php $__currentLoopData = $properties; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $property): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="realty_list_item mb-2">
                            <div class="realty_list_item_actions_stats">
                                <img src="<?php echo e($property->cover()); ?>" alt="">
                                <ul>
                                    <?php if($property->sale == true && !empty($property->sale_price)): ?>
                                    <li>Venda: <?php echo e($property->sale_price); ?></li>
                                    <?php endif; ?>
    
                                    <?php if($property->rent == true && !empty($property->rent_price)): ?>
                                    <li>Aluguel: <?php echo e($property->rent_price); ?></li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                            <div class="realty_list_item_content">
                                <h4># <?php echo e($property->id); ?> - <?php echo e($property->category); ?> - <?php echo e($property->type); ?></h4>
    
                                <div class="realty_list_item_card">
                                    <div class="realty_list_item_card_image">
                                        <span class="icon-realty-location"></span>
                                    </div>
                                    <div class="realty_list_item_card_content">
                                        <span class="realty_list_item_description_title">Bairro:</span>
                                        <span
                                            class="realty_list_item_description_content"><?php echo e($property->neighborhood); ?></span>
                                    </div>
                                </div>
                                <div class="realty_list_item_card">
                                    <div class="realty_list_item_card_image">
                                        <span class="icon-realty-util-area"></span>
                                    </div>
                                    <div class="realty_list_item_card_content">
                                        <span class="realty_list_item_description_title">Área
                                            Útil:</span>
                                        <span
                                            class="realty_list_item_description_content"><?php echo e($property->area_util); ?>&sup2;</span>
                                    </div>
                                </div>
                                <div class="realty_list_item_card">
                                    <div class="realty_list_item_card_image">
                                        <span class="icon-realty-bed"></span>
                                    </div>
                                    <div class="realty_list_item_card_content">
                                        <span
                                            class="realty_list_item_description_title">Domitórios:</span>
                                        <span class="realty_list_item_description_content"><?php echo e($property->bedrooms + $property->suites); ?>

                                            Quartos<br><span> sendo <?php echo e($property->suites); ?> suite(s)</span></span>
                                    </div>
                                </div>
                                <div class="realty_list_item_card">
                                    <div class="realty_list_item_card_image">
                                        <span class="icon-realty-garage"></span>
                                    </div>
                                    <div class="realty_list_item_card_content">
                                        <span
                                            class="realty_list_item_description_title">Garagem:</span>
                                        <span class="realty_list_item_description_content"><?php echo e($property->garage + $property->garage_covered); ?>

                                            Vagas<br><span>Sendo <?php echo e($property->garage_covered); ?> cobertas</span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="realty_list_item_actions">
                                <ul>
                                    <li class="icon-eye">1234 Visualizações</li>
                                </ul>
                                <div>
                                    <a href="" class="btn btn-blue icon-eye">Visualizar Imóvel</a>
                                    <a href="<?php echo e(route('admin.properties.edit',['property'=>$property->id ])); ?>" class="btn btn-green icon-pencil-square-o">Editar
                                        Imóvel</a>
                                </div>
                            </div>
                        </div> 
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?> 
                            <div class="no-content">Não foram encontrados registros!</div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('admin.master.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>