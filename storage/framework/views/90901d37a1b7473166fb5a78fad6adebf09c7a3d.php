<?php $__env->startSection('content'); ?>
    

<section class="dash_content_app">

    <header class="dash_content_app_header">
        <h2 class="icon-search">Filtro</h2>

        <div class="dash_content_app_header_actions">
            <nav class="dash_content_app_breadcrumb">
                <ul>
                    <li><a href="<?php echo e(route('admin.home')); ?>">Dashboard</a></li>
                    <li class="separator icon-angle-right icon-notext"></li>
                    <li><a href="<?php echo e(route('admin.contracts.index')); ?>">Contratos</a></li>
                   
                </ul>
            </nav>

            <a href="<?php echo e(route('admin.contracts.create')); ?>" class="btn btn-orange icon-file-text ml-1">Criar Contrato</a>
            <button class="btn btn-green icon-search icon-notext ml-1 search_open"></button>
        </div>
    </header>

    <?php echo $__env->make('admin.contracts.filter', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>;

    
    <div class="dash_content_app_box">
        <div class="dash_content_app_box_stage">
            <table id="dataTable" class="nowrap hover stripe" width="100" style="width: 100% !important;">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Locador</th>
                    <th>Locatário</th>
                    <th>Negócio</th>
                    <th>Início</th>
                    <th>Vigência</th>
                </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $contracts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $contract): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><a href="<?php echo e(route('admin.contracts.edit',['contract'=>$contract->id])); ?>"  class="text-orange"><?php echo e($contract->id); ?></a></td>
                        <td><a href="<?php echo e(route('admin.users.edit',['user' =>$contract->ownerObject->id])); ?>" class="text-orange"><?php echo e($contract->ownerObject->name); ?></a></td>
                        <td><a href="<?php echo e(route('admin.users.edit',['user' =>$contract->acquirerObject->id])); ?>" class="text-orange"><?php echo e($contract->acquirerObject->name); ?></a></td>
                        <td> <?php echo e(($contract->sale == true ? 'Venda' : 'Locação')); ?> </td>
                        <td><?php echo e($contract->start_at); ?></td>
                        <td><?php echo e($contract->deadline); ?></td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>