<?php $__env->startSection('content'); ?>
    <section class="dash_content_app">

        <header class="dash_content_app_header">
            <h2 class="icon-user-plus">Editar Cliente</h2>

            <div class="dash_content_app_header_actions">
                <nav class="dash_content_app_breadcrumb">
                    <ul>
                        <li><a href="<?php echo e(route('admin.home')); ?>">Dashboard</a></li>
                        <li class="separator icon-angle-right icon-notext"></li>
                        <li><a href="<?php echo e(route('admin.users.index')); ?>">Clientes</a></li>
                        <li class="separator icon-angle-right icon-notext"></li>
                        <li><a href="<?php echo e(route('admin.users.create')); ?>" class="text-orange">Novo Cliente</a></li>
                    </ul>
                </nav>
            </div>
        </header>

        <div class="dash_content_app_box">

            <div class="nav">
                <?php if($errors->all()): ?>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $__env->startComponent('admin.components.message', ['color'=>'orange']); ?>
                        <p class="icon-asterisk"> <?php echo e($error); ?></p>
                        <?php echo $__env->renderComponent(); ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
                <?php if(session()->exists('message')): ?>
                    <?php $__env->startComponent('admin.components.message', ['color'=>session()->get('color')]); ?>
                    <p class="icon-asterisk"><?php echo e(session()->get('message')); ?></p>
                    <?php echo $__env->renderComponent(); ?>

                <?php endif; ?>
                <ul class="nav_tabs">
                    <li class="nav_tabs_item">
                        <a href="#data" class="nav_tabs_item_link active">Dados Cadastrais</a>
                    </li>
                    <li class="nav_tabs_item">
                        <a href="#complementary" class="nav_tabs_item_link">Dados Complementares</a>
                    </li>
                    <li class="nav_tabs_item">
                        <a href="#realties" class="nav_tabs_item_link">Imóveis</a>
                    </li>
                    <li class="nav_tabs_item">
                        <a href="#management" class="nav_tabs_item_link">Administrativo</a>
                    </li>
                </ul>

                <form class="app_form" action="<?php echo e(route('admin.users.update', ['user' => $user->id])); ?>"
                    method="post" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <?php echo method_field('PUT'); ?>
                    <input type="hidden" name="id" value="<?php echo e($user->id); ?>">
                    <div class="nav_tabs_content">
                        <div id="data">
                            <div class="label_gc">
                                <span class="legend">Perfil:</span>
                                <label class="label">
                                    <input type="checkbox" name="lessor"
                                        <?php echo e(old('lessor') == 'on' || old('lessor') == true ? 'checked' : ''); ?>><span>Locatário</span>
                                </label>

                                <label class="label">
                                    <input type="checkbox" name="lessee"
                                        <?php echo e(old('lessee') == 'on' || old('lessee') == true ? 'checked' : ''); ?>><span>Locador</span>
                                </label>
                            </div>

                            <label class="label">
                                <span class="legend">*Nome:</span>
                                <input type="text" name="name" placeholder="Nome Completo"
                                    value="<?php echo e(old('name') ?? $user->name); ?>" />
                            </label>

                            <div class="label_g2">
                                <label class="label">
                                    <span class="legend">*Genero:</span>
                                    <select name="genre">
                                        <option value="male" <?php echo e(old('genre') === 'male' ? 'selected' : ''); ?>>Masculino
                                        </option>
                                        <option value="female" <?php echo e(old('genre') === 'female' ? 'selected' : ''); ?>>Feminino
                                        </option>
                                        <option value="other" <?php echo e(old('genre') === 'other' ? 'selected' : ''); ?>>Outros
                                        </option>
                                    </select>
                                </label>

                                <label class="label">
                                    <span class="legend">*CPF:</span>
                                    <input type="text" class="mask-doc" name="document" placeholder="CPF do Cliente"
                                        value="<?php echo e(old('document') ?? $user->document); ?>" />
                                </label>
                            </div>

                            <div class="label_g2">
                                <label class="label">
                                    <span class="legend">*RG:</span>
                                    <input type="text" name="document_secondary" placeholder="RG do Cliente"
                                        value="<?php echo e(old('document_secondary') ?? $user->document_secondary); ?>" />
                                </label>

                                <label class="label">
                                    <span class="legend">Órgão Expedidor:</span>
                                    <input type="text" name="document_secondary_complement" placeholder="Expedição"
                                        value="<?php echo e(old('document_secondary_complement') ?? $user->document_secondary_complement); ?>" />
                                </label>
                            </div>

                            <div class="label_g2">
                                <label class="label">
                                    <span class="legend">*Data de Nascimento:</span>
                                    <input type="tel" name="date_of_birth" class="mask-date"
                                        placeholder="Data de Nascimento"
                                        value="<?php echo e(old('date_of_birth') ?? $user->date_of_birth); ?>" />
                                </label>

                                <label class="label">
                                    <span class="legend">*Naturalidade:</span>
                                    <input type="text" name="place_of_birth" placeholder="Cidade de Nascimento"
                                        value="<?php echo e(old('place_of_birth') ?? $user->place_of_birth); ?>" />
                                </label>
                            </div>

                            <div class="label_g2">
                                <label class="label">
                                    <span class="legend">*Estado Civil:</span>
                                    <select name="civil_status">
                                        <optgroup label="Cônjuge Obrigatório">
                                            <option value="married"
                                                <?php echo e(old('civil_status') === 'married' ? 'selected' : ''); ?>>Casado</option>
                                            <option value="separated"
                                                <?php echo e(old('civil_status') === 'separated' ? 'selected' : ''); ?>>Separado
                                            </option>
                                        </optgroup>
                                        <optgroup label="Cônjuge não Obrigatório">
                                            <option value="single"
                                                <?php echo e(old('civil_status') === 'single' ? 'selected' : ''); ?>>
                                                Solteiro</option>
                                            <option value="divorced"
                                                <?php echo e(old('civil_status') === 'divorced' ? 'selected' : ''); ?>>Divorciado
                                            </option>
                                            <option value="widower"
                                                <?php echo e(old('civil_status') === 'widower' ? 'selected' : ''); ?>>Viúvo</option>
                                        </optgroup>
                                    </select>
                                </label>

                                <label class="label">
                                    <span class="legend">Foto</span>
                                    <input type="file" name="cover">
                                </label>
                            </div>

                            <div class="app_collapse mt-2">
                                <div class="app_collapse_header collapse">
                                    <h3>Renda</h3>
                                    <span class="icon-plus-circle icon-notext"></span>
                                </div>

                                <div class="app_collapse_content d-none">
                                    <div class="label_g2">
                                        <label class="label">
                                            <span class="legend">*Profissão:</span>
                                            <input type="text" name="occupation" placeholder="Profissão do Cliente"
                                                value="<?php echo e(old('occupation') ?? $user->occupation); ?>" />
                                        </label>

                                        <label class="label">
                                            <span class="legend">*Renda:</span>
                                            <input type="tel" name="income" class="mask-money"
                                                placeholder="Valores em Reais"
                                                value="<?php echo e(old('income') ?? $user->income); ?>" />
                                        </label>
                                    </div>

                                    <label class="label">
                                        <span class="legend">*Empresa:</span>
                                        <input type="text" name="company_work" placeholder="Contratante"
                                            value="<?php echo e(old('company_work') ?? $user->company_work); ?>" />
                                    </label>
                                </div>
                            </div>

                            <div class="app_collapse mt-2">
                                <div class="app_collapse_header collapse">
                                    <h3>Endereço</h3>
                                    <span class="icon-plus-circle icon-notext"></span>
                                </div>

                                <div class="app_collapse_content d-none">
                                    <div class="label_g2">
                                        <label class="label">
                                            <span class="legend">*CEP:</span>
                                            <input type="text" name="zipcode" class="mask-zipcode zip_code_search"
                                                placeholder="Digite o CEP"
                                                value="<?php echo e(old('zipcode') ?? $user->zipcode); ?>" />
                                        </label>
                                    </div>

                                    <label class="label">
                                        <span class="legend">*Endereço:</span>
                                        <input type="text" name="street" class="street"
                                            placeholder="Endereço Completo"
                                            value="<?php echo e(old('street') ?? $user->street); ?>" />
                                    </label>

                                    <div class="label_g2">
                                        <label class="label">
                                            <span class="legend">*Número:</span>
                                            <input type="text" name="number" placeholder="Número do Endereço"
                                                value="<?php echo e(old('number') ?? $user->number); ?>" />
                                        </label>

                                        <label class="label">
                                            <span class="legend">Complemento:</span>
                                            <input type="text" name="complement" placeholder="Completo (Opcional)"
                                                value="<?php echo e(old('complement') ?? $user->complement); ?>" />
                                        </label>
                                    </div>

                                    <label class="label">
                                        <span class="legend">*Bairro:</span>
                                        <input type="text" name="neighborhood" class="neighborhood" placeholder="Bairro"
                                            value="<?php echo e(old('neighborhood') ?? $user->neighborhood); ?>" />
                                    </label>

                                    <div class="label_g2">
                                        <label class="label">
                                            <span class="legend">*Estado:</span>
                                            <input type="text" name="state" class="state" placeholder="Estado"
                                                value="<?php echo e(old('state') ?? $user->state); ?>" />
                                        </label>

                                        <label class="label">
                                            <span class="legend">*Cidade:</span>
                                            <input type="text" name="city" class="city" placeholder="Cidade"
                                                value="<?php echo e(old('city') ?? $user->city); ?>" />
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="app_collapse mt-2">
                                <div class="app_collapse_header collapse">
                                    <h3>Contato</h3>
                                    <span class="icon-plus-circle icon-notext"></span>
                                </div>

                                <div class="app_collapse_content d-none">
                                    <div class="label_g2">
                                        <label class="label">
                                            <span class="legend">Residencial:</span>
                                            <input type="tel" name="telephone" class="mask-phone"
                                                placeholder="Número do Telefonce com DDD"
                                                value="<?php echo e(old('telephone') ?? $user->telephone); ?>" />
                                        </label>

                                        <label class="label">
                                            <span class="legend">*Celular:</span>
                                            <input type="tel" name="cell" class="mask-cell"
                                                placeholder="Número do Telefonce com DDD"
                                                value="<?php echo e(old('cell') ?? $user->cell); ?>" />
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="app_collapse mt-2">
                                <div class="app_collapse_header collapse">
                                    <h3>Acesso</h3>
                                    <span class="icon-plus-circle icon-notext"></span>
                                </div>

                                <div class="app_collapse_content d-none">
                                    <div class="label_g2">
                                        <label class="label">
                                            <span class="legend">*E-mail:</span>
                                            <input type="email" name="email" placeholder="Melhor e-mail"
                                                value="<?php echo e(old('email') ?? $user->email); ?>" />
                                        </label>

                                        <label class="label">
                                            <span class="legend">Senha:</span>
                                            <input type="password" name="password" placeholder="Senha de acesso" value="" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="complementary" class="d-none">
                            <div class="app_collapse">
                                <div class="app_collapse_header collapse">
                                    <h3>Cônjuge</h3>
                                    <span class="icon-plus-circle icon-notext"></span>
                                </div>

                                <div class="app_collapse_content d-none content_spouse">

                                    <label class="label">
                                        <span class="legend">Tipo de Comunhão:</span>
                                        <select name="type_of_communion" class="select2">
                                            <option value="Comunhão Universal de Bens"
                                                <?php echo e(old('type_of_communion') === 'Comunhão Universal de Bens"' ? 'selected' : ''); ?>>
                                                Comunhão Universal de Bens
                                            </option>
                                            <option value="Comunhão Parcial de Bens"
                                                <?php echo e(old('type_of_communion') === 'Comunhão Parcial de Bens' ? 'selected' : ''); ?>>
                                                Comunhão Parcial de Bens</option>
                                            <option value="Separação Total de Bens"
                                                <?php echo e(old('type_of_communion') === 'Separação Total de Bens"' ? 'selected' : ''); ?>>
                                                Separação Total de Bens</option>
                                            <option value="Participação Final de Aquestos"
                                                <?php echo e(old('type_of_communion') === 'Participação Final de Aquestos' ? 'selected' : ''); ?>>
                                                Participação Final de
                                                Aquestos
                                            </option>
                                        </select>
                                    </label>

                                    <label class="label">
                                        <span class="legend">Nome:</span>
                                        <input type="text" name="spouse_name" placeholder="Nome do Cônjuge"
                                            value="<?php echo e(old('spouse_name') ?? $user->spouse_name); ?>" />
                                    </label>

                                    <div class="label_g2">
                                        <label class="label">
                                            <span class="legend">Genero:</span>
                                            <select name="spouse_genre">
                                                <option value="male"
                                                    <?php echo e(old('spouse_genre') === 'male' ? 'selected' : ''); ?>>
                                                    Masculino</option>
                                                <option value="female"
                                                    <?php echo e(old('spouse_genre') === 'female' ? 'selected' : ''); ?>>Feminino
                                                </option>
                                                <option
                                                    value="other <?php echo e(old('spouse_genre') === 'other' ? 'selected' : ''); ?>">
                                                    Outros</option>
                                            </select>
                                        </label>

                                        <label class="label">
                                            <span class="legend">CPF:</span>
                                            <input type="text" class="mask-doc" name="spouse_document"
                                                placeholder="CPF do Cliente"
                                                value="<?php echo e(old('spouse_document') ?? $user->spouse_document); ?>" />
                                        </label>
                                    </div>

                                    <div class="label_g2">
                                        <label class="label">
                                            <span class="legend">RG:</span>
                                            <input type="text" name="spouse_document_secondary" placeholder="RG do Cliente"
                                                value="<?php echo e(old('spouse_document_secondary') ?? $user->spouse_document_secondary); ?>" />
                                        </label>

                                        <label class="label">
                                            <span class="legend">Órgão Expedidor:</span>
                                            <input type="text" name="spouse_document_secondary_complement"
                                                placeholder="Expedição"
                                                value="<?php echo e(old('spouse_document_secondary_complement') ?? $user->spouse_document_secondary_complement); ?>" />
                                        </label>
                                    </div>

                                    <div class="label_g2">
                                        <label class="label">
                                            <span class="legend">Data de Nascimento:</span>
                                            <input type="text" class="mask-date" name="spouse_date_of_birth"
                                                placeholder="Data de Nascimento"
                                                value="<?php echo e(old('spouse_date_of_birth') ?? $user->spouse_date_of_birth); ?>" />
                                        </label>

                                        <label class="label">
                                            <span class="legend">Naturalidade:</span>
                                            <input type="text" name="spouse_place_of_birth"
                                                placeholder="Cidade de Nascimento"
                                                value="<?php echo e(old('spouse_place_of_birth') ?? $user->spouse_place_of_birth); ?>" />
                                        </label>
                                    </div>

                                    <div class="label_g2">
                                        <label class="label">
                                            <span class="legend">Profissão:</span>
                                            <input type="text" name="spouse_occupation" placeholder="Profissão do Cliente"
                                                value="<?php echo e(old('spouse_occupation') ?? $user->spouse_occupation); ?>" />
                                        </label>

                                        <label class="label">
                                            <span class="legend">Renda:</span>
                                            <input type="text" class="mask-money" name="spouse_income"
                                                placeholder="Valores em Reais"
                                                value="<?php echo e(old('spouse_income') ?? $user->spouse_income); ?>" />
                                        </label>
                                    </div>

                                    <label class="label">
                                        <span class="legend">Empresa:</span>
                                        <input type="text" name="spouse_company_work" placeholder="Contratante"
                                            value="<?php echo e(old('spouse_company_work') ?? $user->spouse_company_work); ?>" />
                                    </label>
                                </div>
                            </div>

                            <div class="app_collapse mt-2">
                                <div class="app_collapse_header collapse">
                                    <h3>Empresa</h3>
                                    <span class="icon-minus-circle icon-notext"></span>
                                </div>

                                <div class="app_collapse_content">

                                    <div class="companies_list">

                                        <?php if($user->companies()->get()): ?>
                                            <?php $__currentLoopData = $user->companies()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                
                                        <div class="companies_list_item mb-2">
                                            <p><b>Razão Social:</b> <?php echo e($company->social_name); ?></p>
                                            <p><b>Nome Fantasia:</b><?php echo e($company->alias_name); ?></p>
                                            <p><b>CNPJ:</b> <?php echo e($company->document_company); ?> - <b>Inscrição Estadual:</b><?php echo e($company->document_company_secondary); ?>

                                            </p>
                                            <p><b>Endereço:</b> <?php echo e($company->street); ?>  , <?php echo e($company->number); ?> , <?php echo e($company->complement); ?> </p>
                                            <p><b>CEP:</b> <?php echo e($company->zipcode); ?> <b>Bairro:</b> <?php echo e($company->neighborhood); ?> <b>Cidade/Estado:</b>
                                                <?php echo e($company->city); ?>/<?php echo e($company->state); ?></p>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    
                                        <?php else: ?> 
                                            <div class="no-content mb-2">Não foram encontrados registros!</div>
                                        </div>

                                        <?php endif; ?>
                                  
                                    </div>
                                    <p class="text-right">
                                        <a href="<?php echo e(route('admin.companies.create', ['user'=>$user->id])); ?>"
                                            class="btn btn-green  icon-building-o">Cadastrar
                                            Nova Empresa</a>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div id="realties" class="d-none">
                            <div class="app_collapse">
                                <div class="app_collapse_header collapse">
                                    <h3>Locador</h3>
                                    <span class="icon-minus-circle icon-notext"></span>
                                </div>

                                <div class="app_collapse_content">
                                    <div id="realties">
                                        <div class="realty_list">
                                            <?php if($user->properties()->get()): ?>
                                            <?php $__currentLoopData = $user->properties()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $property): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="realty_list_item mb-1"> 
                                                <div class="realty_list_item_actions_stats">
                                                   <img src="<?php echo e($property->cover()); ?>" alt="">
                                                   <ul>
                                                    <?php if($property->sale == true && !empty($property->sale_price)): ?>
                                                    <li>Venda: <?php echo e($property->sale_price); ?></li>
                                                    <?php endif; ?>
                    
                                                    <?php if($property->rent == true && !empty($property->rent_price)): ?>
                                                    <li>Aluguel: <?php echo e($property->rent_price); ?></li>
                                                    <?php endif; ?>
                                                </ul>
                                                </div>

                                                <div class="realty_list_item_content">
                                                    <h4># <?php echo e($property->id); ?> - <?php echo e($property->category); ?> - <?php echo e($property->type); ?></h4>

                                                    <div class="realty_list_item_card">
                                                        <div class="realty_list_item_card_image">
                                                            <span class="icon-realty-location"></span>
                                                        </div>
                                                        <div class="realty_list_item_card_content">
                                                            <span class="realty_list_item_description_title">Bairro:</span>
                                                            <span
                                                                class="realty_list_item_description_content"><?php echo e($property->neighborhood); ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="realty_list_item_card">
                                                        <div class="realty_list_item_card_image">
                                                            <span class="icon-realty-util-area"></span>
                                                        </div>
                                                        <div class="realty_list_item_card_content">
                                                            <span class="realty_list_item_description_title">Área
                                                                Útil:</span>
                                                            <span
                                                                class="realty_list_item_description_content"><?php echo e($property->area_util); ?>&sup2;</span>
                                                        </div>
                                                    </div>

                                                    <div class="realty_list_item_card">
                                                        <div class="realty_list_item_card_image">
                                                            <span class="icon-realty-bed"></span>
                                                        </div>
                                                        <div class="realty_list_item_card_content">
                                                            <span
                                                                class="realty_list_item_description_title">Domitórios:</span>
                                                            <span class="realty_list_item_description_content"><?php echo e($property->bedrooms + $property->suites); ?>

                                                                Quartos<br><span> sendo <?php echo e($property->suites); ?> suite(s)</span></span>
                                                        </div>
                                                    </div>

                                                    <div class="realty_list_item_card">
                                                        <div class="realty_list_item_card_image">
                                                            <span class="icon-realty-garage"></span>
                                                        </div>
                                                        <div class="realty_list_item_card_content">
                                                            <span
                                                                class="realty_list_item_description_title">Garagem:</span>
                                                            <span class="realty_list_item_description_content"><?php echo e($property->garage + $property->garage_covered); ?>

                                                                Vagas<br><span>Sendo <?php echo e($property->garage_covered); ?> cobertas</span></span>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="realty_list_item_actions">
                                                    <ul>
                                                        <li class="icon-eye">1234 Visualizações</li>
                                                    </ul>
                                                    <div>
                                                        <a href="" class="btn btn-blue icon-eye">Visualizar Imóvel</a>
                                                        <a href="<?php echo e(route('admin.properties.edit',['property'=>$property->id ])); ?>" class="btn btn-green icon-pencil-square-o">Editar
                                                            Imóvel</a>
                                                    </div>
                                                </div>
                                            </div> 
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          
                                                <?php else: ?> 
                                                <div class="no-content">Não foram encontrados registros!</div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="app_collapse mt-3">
                                <div class="app_collapse_header collapse">
                                    <h3>Locatário</h3>
                                    <span class="icon-minus-circle icon-notext"></span>
                                </div>

                                <div class="app_collapse_content">
                                    <div id="realties">
                                        <div class="no-content">Não foram encontrados registros!</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="management" class="d-none">
                            <div class="label_gc">
                                <span class="legend">Conceder:</span>
                                <label class="label">
                                    <input type="checkbox" name="admin"
                                        <?php echo e(old('admin') == 'on' || old('admin') == true ? 'checked' : ''); ?>><span>Administrativo</span>
                                </label>

                                <label class="label">
                                    <input type="checkbox" name="client"
                                        <?php echo e(old('client') == 'on' || old('client') == true ? 'checked' : ''); ?>><span>Cliente</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="text-right mt-2">
                        <button class="btn btn-large btn-green icon-check-square-o" type="submit">Salvar Alterações
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>



<?php echo $__env->make('admin.master.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>